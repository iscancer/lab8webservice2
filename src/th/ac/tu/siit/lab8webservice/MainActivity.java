package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;

import org.json.*;

import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

import android.content.DialogInterface;
import android.content.Intent;



public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String location;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Do not allow user to change application orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
		
		File infile = getBaseContext().getFileStreamPath("location.tsv");
		if (infile.exists()) {
			try {
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					String line = sc.nextLine();
					location = line;
					
				}
				sc.close();
			} catch (FileNotFoundException e) {
				//Do nothing
			}
		}
		
	
		
		
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		//CHECK IF DEVICE HAS INTERNET
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//LOAD DATA WHEN THERE IS CONNECTION
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 5*60*1000) {
				//NEW THREAD, takes a while
				WeatherTask task = new WeatherTask(this);	
				String locURL = "http://cholwich.org/"+location+".json";
				/*
				Toast t = Toast.makeText(this,locURL,Toast.LENGTH_LONG);
				t.show();
				*/
				//task.execute("http://cholwich.org/bangkok.json");
				task.execute(locURL);
			}
			// UI Threasds versuse Background threads (time-consuming tasks)
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		WeatherTask task = new WeatherTask(this);
		switch(id) {
		case R.id.opt1:	
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("nonthaburi");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="nonthaburi";
			task.execute("http://cholwich.org/nonthaburi.json");		
			return true;			
		case R.id.opt2:
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("bangkok");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="bangkok";
			task.execute("http://cholwich.org/bangkok.json");
			return true;
		case R.id.opt3:
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("pathumthani");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="pathumthani";
			task.execute("http://cholwich.org/pathumthani.json");
			return true;
		case R.id.refresh:
			//refresh

			long current = System.currentTimeMillis();
			//modified to be 1 minute
			if (current - lastUpdate > 10*1000) {
				String locURL = "http://cholwich.org/"+location+".json";
				Toast t = Toast.makeText(this, "REFRESHED! HAPPY?", 
									Toast.LENGTH_SHORT);
				task.execute(locURL);
			}else{
				Toast t = Toast.makeText(this, "CALM DOWN, IT'S ONLY BEEN "
			+(current-lastUpdate)/1000+" SECONDS SINCE YOU LAST REFRESHED", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	//executed in UI thread
	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		@Override
		//EXCECUTED IN UI
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			setTitle(location+" Weather");
			//setTitle("Bangkok Weather");
		}
		//excecuted in background
		
		
		
		
		
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//get first parameter, set as url
				URL url = new URL(params[0]);
				//create connection to URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//download data from URL
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					JSONObject jwind = json.getJSONObject("wind");
					list.clear();
					
					//add "description"
					JSONArray jweather = json.getJSONArray("weather");
					//In case of more than one whather. get at index 0
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", description);
					
					list.add(record);	
					
					//temp
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					//max/min temp
					record = new HashMap<String,String>();
					record.put("name", "Temp Min/Max");
					double temp_min = jmain.getDouble("temp_min")-273.0;
					double temp_max = jmain.getDouble("temp_max")-273.0;
					String tempminmax = String.format(Locale.getDefault(), "Min = %.1f", temp_min)+", "+String.format(Locale.getDefault(), "Max = %.1f", temp_max);
					record.put("value", tempminmax);
					list.add(record);
					
					//pressure
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					double pressu = jmain.getDouble("pressure");
					record.put("value", ""+pressu+" hPa");
					list.add(record);
					
					//humidity
					record = new HashMap<String,String>();
					record.put("name", "Humidity");
					double humi = jmain.getDouble("humidity");
					record.put("value", ""+humi+"%");
					list.add(record);
					
					
					//humidity
					record = new HashMap<String,String>();
					record.put("name", "Wind Speed");
					double speed = jwind.getDouble("speed");
					record.put("value", ""+speed+" mps");
					list.add(record);
					
					//humidity
					record = new HashMap<String,String>();
					record.put("name", "Wind Degree");
					double deg = jwind.getDouble("deg");
					record.put("value", ""+deg+" degrees");
					list.add(record);
					
					//when run, it will show description of weather.
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

